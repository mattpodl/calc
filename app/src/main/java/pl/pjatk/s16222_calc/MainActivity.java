package pl.pjatk.s16222_calc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ArrayList<Button> buttons;
    private static final int[] BUTTON_IDS = {
            R.id.button_bmi_calculator,
            R.id.button_corona_chart,
            R.id.button_harris,
            R.id.button_quiz,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialiseButtons();
    }

    private void initialiseButtons() {
        buttons = new ArrayList<>(BUTTON_IDS.length);
        for (int id : BUTTON_IDS) {
            Button button = findViewById(id);
            button.setOnClickListener(this);
            buttons.add(button);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_bmi_calculator:
                changeActivity(BmiActivity.class);
                break;

            case R.id.button_corona_chart:
                changeActivity(CoronaChart.class);
                break;

            case R.id.button_harris:
                changeActivity(HarrisBenedictActivity.class);
                break;
            case R.id.button_quiz:
                changeActivity(Quize.class);
                break;
        }
    }

    private void changeActivity(Class<?> cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }


}


