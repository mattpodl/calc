package pl.pjatk.s16222_calc

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Answer(var rightOne: Boolean, var text: String)
class Question(var question: String, var answers: List<Answer>)

// question & answers from https://www.newsweek.pl/swiat/koronawirus-10-faktow-ktore-juz-o-nim-wiemy/hgq36k6
class Quize : AppCompatActivity(), View.OnClickListener {

    private val answersPerQuestion = 3
    private var currentQuestionNumber: Int = 0
    private var rightAnswersTotal: Int = 0

    lateinit var restartButton: Button
    lateinit var answersButtons: List<Button>
    lateinit var questionTextView: TextView
    var questionList = createQuestions()
    lateinit var currentQuestion: Question

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quize)

        restartButton = findViewById(R.id.buttonRestartQuiz)
        restartButton.setOnClickListener { this.restart() }

        answersButtons = listOf(
                findViewById(R.id.buttonA),
                findViewById(R.id.buttonB),
                findViewById(R.id.buttonC),
                findViewById(R.id.buttonD)
        )
        answersButtons.forEach { b -> b.setOnClickListener(this) }
        questionTextView = findViewById(R.id.QuestionTextView)

        questionList.shuffle()
        askQuestion(currentQuestionNumber)
    }

    override fun onClick(v: View?) {
        val answerText = findViewById<Button>(v!!.id).text;
        val isAnswerCorrect = currentQuestion.answers.first { it.text == answerText }.rightOne
        if (isAnswerCorrect){
            rightAnswersTotal++
        }
        currentQuestionNumber += 1
        askQuestion(currentQuestionNumber)
    }
    
    private fun askQuestion(questionNumber: Int) {
        if(questionNumber >= 1 )
        currentQuestion = questionList[questionNumber]
        singleQuestion(currentQuestion)
    }

    private fun singleQuestion(question: Question) {
        questionTextView.text = question.question
        for (x in 0..answersPerQuestion) {
            answersButtons[x].text = question.answers[x].text
        }
    }

    private fun restart() {
        restartButton.visibility = View.VISIBLE
    }

    private fun showResult() {

    }

    private fun createQuestions(): MutableList<Question> {
        val list = mutableListOf<Question>()
        list += Question("Kiedy bedzie szczepionka na Covid-19", listOf(Answer(false, "Jest juz"), Answer(false, "Nigdy"), Answer(true, "nie wiadomo"), Answer(false, "jutro")))
        list += Question("Gdzie zaczęła się epidemia?", listOf(Answer(false, "Wun"), Answer(false, "Wan"), Answer(true, "Wuhan"), Answer(false, "Japonia")))
        list += Question("Kiedy odkryto pierwszy przypadek w Polsce", listOf(Answer(false, "luty"), Answer(true, "marzec"), Answer(false, "kwiecień"), Answer(false, "maj")))
        list += Question("Od kiedy loty z Wuhan sa wstrzymane", listOf(Answer(false, "Grudzień"), Answer(false, "Luty"), Answer(true, "Styczeń"), Answer(false, "Nigdy")))
        return list
    }


}
