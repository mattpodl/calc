package pl.pjatk.s16222_calc

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

import androidx.appcompat.app.AppCompatActivity

class BmiActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bmi)

        val heightEditText = findViewById<EditText>(R.id.BodyHeight)
        val weightEditText = findViewById<EditText>(R.id.BodyWeight)
        val resultTextView = findViewById<TextView>(R.id.ResultTextView)
        val buttonCalculate = findViewById<Button>(R.id.ButtonCalculate)
        buttonCalculate.setOnClickListener { this.calculate(heightEditText, weightEditText, resultTextView) }
    }

    private fun changeToMeters(value: Double): Double {
        return value / 100
    }

    private fun calculate(heightEditText: EditText, WeightEditText: EditText, ResultTextView: TextView) {
        val weight = java.lang.Double.parseDouble(WeightEditText.text.toString())
        val height = changeToMeters(java.lang.Double.parseDouble(heightEditText.text.toString()))
        val value = weight / (height * height)
        ResultTextView.text = value.toString()
    }

}
