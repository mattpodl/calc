package pl.pjatk.s16222_calc

import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView

class HarrisBenedictActivity : AppCompatActivity() {

    private lateinit var HeightEditText: EditText
    private lateinit var WeightEditText: EditText
    private lateinit var AgeEditText: EditText

    private lateinit var ResultTextView: TextView
    private lateinit var RecipeTextView: TextView

    private lateinit var radioGroup: RadioGroup
    private lateinit var calculateButton: Button
    private lateinit var returnButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_harris_benedict)
        HeightEditText = findViewById(R.id.editTextHeight)
        WeightEditText = findViewById(R.id.editTextWeight)
        AgeEditText = findViewById(R.id.editTextAge)
        ResultTextView = findViewById(R.id.PPM)
        RecipeTextView = findViewById(R.id.recipeTextView)
        radioGroup = findViewById(R.id.radioSex)
        calculateButton = findViewById(R.id.button_harris_calculate)
        returnButton = findViewById(R.id.backButton)
        calculateButton.setOnClickListener { Calculate() }
        returnButton.setOnClickListener { finish() }

    }

    private fun Calculate() {
        val femaleFactors = doubleArrayOf(655.1, 9.563, 1.85, 4.676)
        val maleFactors = doubleArrayOf(66.5, 13.75, 5.003, 6.775)
        val weight: Int
        val height: Int
        val age: Int
        var ppm = 0

        try {
            weight = Integer.parseInt(WeightEditText.text.toString())
            height = Integer.parseInt(HeightEditText.text.toString())
            age = Integer.parseInt(AgeEditText.text.toString())
        } catch (ex: Exception) {
            ResultTextView.text = getString(R.string.errorImputNotValid)
            RecipeTextView.text = getString(R.string.recipePlaceholder)
            return
        }

        when (radioGroup.checkedRadioButtonId) {
            R.id.female -> ppm = calculatePPM(femaleFactors, weight, height, age).toInt()
            R.id.male -> ppm = calculatePPM(maleFactors, weight, height, age).toInt()
        }
        ResultTextView.text = """$ppm"""
        when {
            ppm < 1000 -> RecipeTextView.setText(R.string.thousandandless)
            ppm < 2000 -> RecipeTextView.setText(R.string.thousandtotwothousands)
            else -> RecipeTextView.setText(R.string.twothousandsandmore)
        }
    }

    private fun calculatePPM(factors: DoubleArray, bodyWeight: Int, bodyHeight: Int, age: Int): Double {
        return factors[0] + factors[1] * bodyWeight + factors[2] * bodyHeight - factors[3] * age
    }
}
