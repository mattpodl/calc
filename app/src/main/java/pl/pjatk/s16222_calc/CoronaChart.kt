package pl.pjatk.s16222_calc

import android.os.Bundle
import android.view.View
import android.widget.Button

import androidx.appcompat.app.AppCompatActivity

import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.charts.Cartesian

import java.util.ArrayList

class CoronaChart : AppCompatActivity(), View.OnClickListener {


    private var backButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_corona_chart)
        backButton = findViewById(R.id.button_back)
        backButton!!.setOnClickListener(this)

        val myChart = AnyChart.column()

        val data = ArrayList<DataEntry>()
        data.add(ValueDataEntry(getString(R.string.sick), 11395))
        data.add(ValueDataEntry(getString(R.string.healthy), 2265))
        data.add(ValueDataEntry(getString(R.string.death), 526))

        myChart.data(data)
        myChart.title(getString(R.string.chart_title))

        val anyChartView = findViewById<AnyChartView>(R.id.any_chart_view)
        anyChartView.setChart(myChart)
    }


    override fun onClick(v: View) {
        when (v.id) {
            R.id.button_back -> finish()
        }
    }
}
